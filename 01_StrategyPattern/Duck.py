# STRATEGY PATTERN

class Duck:
    def __init__(self):
        self.flyBehavior = FlyBehavior()
        self.quackBehavior = QuackBehavior()
    
    def performQuack(self):
        self.quackBehavior.quack()
        
    def performFly(self):
        self.flyBehavior.fly()
        
    def setFlyBehavior(self, fb):
        self.flyBehavior = fb
    
    def setQuackBehavior(self, qb):
        self.quackBehavior = qb
    
    def swim(self):
        print('All ducks float, even decoys')
    
    
    def display(self):
        pass

    
################# DUCK TYPES
class MallardDuck(Duck):
    def __init__(self):
        self.quackBehavior = Quack()
        self.flyBehavior = FlywithWings()
    
    def display(self):
        print('Im a real Mallard duck')
    
class RedheadDuck(Duck):
    def __init__(self):
        pass
    
    def display(self):
        print('Im a real Redhead duck') 
    
class ModelDuck(Duck):
    def __init__(self):
        self.quackBehavior = MuteQuack()
        self.flyBehavior = FlynoWay()
    
    def display(self):
        print('Im a model duck') 
        
################# DEFINE BEHAVIOR
class FlyBehavior:
    def fly(self):
        pass
    
class QuackBehavior:
    def quack(self):
        pass
    
class FlywithWings(FlyBehavior):
    def fly(self):
        print('Im flying!!') 
    
class FlynoWay(FlyBehavior):
    def fly(self):
        print('I cant fly.') 
        
class FlyRocketPowered(FlyBehavior):
    def fly(self):
        print('Im flying with a rocket!!!!!') 
    
class Quack(QuackBehavior):
    def quack(self):
        print('Quack')
    
class Squeak(QuackBehavior):
    def quack(self):
        print('Squeak')

class MuteQuack(QuackBehavior):
    def quack(self):
        print('<< Silence >>')

md = MallardDuck()
md.performQuack()
md.performFly()

modelduck = ModelDuck()
modelduck.performQuack()
modelduck.performFly()
modelduck.setFlyBehavior(FlyRocketPowered())
modelduck.performFly()