# OBSERVER PATTERN

class Subject:
    def registerObserver(self, o):
        pass

    def removeObserver(self, o):
        pass

    def notifyObservers(self):
        pass


class WeatherData(Subject):

    def __init__(self):
        self.observers = []
        self.temperature = 0
        self.humidity = 0
        self.pressure = 0

    def registerObserver(self, o):
        self.observers.append(o)

    def removeObserver(self, o):
        self.observers.remove(o)

    def notifyObservers(self):
        if len(self.observers)>0:
            for observer in self.observers:
                observer.update(self.temperature, self.humidity, self.pressure)
        else: 
            print('No observers to notify')

    def measurementsChanged(self):
        self.notifyObservers()
        
    def setMeasurements(self, temperature, humidity, pressure):
        self.temperature = temperature
        self.humidity = humidity
        self.pressure = pressure
        self.measurementsChanged()


# Observer Interface
class Observer:
    def update(self, temperature, humidity, pressure):
        pass
    
    def values(self):
        pass


# Display Interface
class DisplayElement:
    def display(self):
        pass


# Displays
class CurrentConditionDisplay(Observer, DisplayElement):
    
    def __init__(self, weatherData):
        self.weatherData = weatherData
        self.weatherData.registerObserver(self)
        
    def update(self, temperature, humidity, pressure):
        self.temperature = temperature
        self.humidity = humidity
        self.pressure = pressure
        self.display()

    def display(self):
        print(f'Current condition: Temperature: {self.temperature}, Humidity: {self.humidity}, Pressure: {self.pressure}')


class StatisticsDisplay(Observer, DisplayElement):
    def __init__(self, weatherData):
        self.weatherData = weatherData
        self.weatherData.registerObserver(self)
        
    def update(self, temperature, humidity, pressure):
        self.temperature = temperature
        self.humidity = humidity
        self.pressure = pressure
        self.display()

    def display(self):
        print(f'Some statistics ...')



class ForecastDisplay(Observer, DisplayElement):
    def __init__(self, weatherData):
        self.weatherData = weatherData
        self.weatherData.registerObserver(self)
        
    def update(self, temperature, humidity, pressure):
        self.temperature = temperature
        self.humidity = humidity
        self.pressure = pressure
        self.display()

    def display(self):
        print(f'Some Forecasting ...')

class HeatIndexDisplay(Observer, DisplayElement):
    
    def __init__(self, weatherData):
        self.weatherData = weatherData
        self.weatherData.registerObserver(self)
        
    def update(self, temperature, humidity, pressure):
        self.temperature = temperature
        self.humidity = humidity
        self.pressure = pressure
        self.heatindex = round(16.923 + 1.85212*10**-1*self.temperature+5.37941*self.humidity, 2)
        self.display()

    def display(self):
        print(f'Heat Index is {self.heatindex}')



class ThirdPartyDisplay(Observer, DisplayElement):
    
    def __init__(self, weatherData):
        self.weatherData = weatherData
        self.weatherData.registerObserver(self)
        
    def update(self, temperature, humidity, pressure):
        self.temperature = temperature
        self.humidity = humidity
        self.pressure = pressure
        self.display()

    def display(self):
        print(f'Third Party Display API')

wd = WeatherData()
currentcond = CurrentConditionDisplay(wd)
statistics = StatisticsDisplay(wd)
forecast = ForecastDisplay(wd)
heatindex = HeatIndexDisplay(wd)

wd.setMeasurements(80, 65, 30.4)
wd.setMeasurements(82, 70, 29.2)
wd.setMeasurements(82, 70, 29.2)
wd.setMeasurements(78, 90, 29.2)
