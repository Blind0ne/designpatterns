# -*- coding: utf-8 -*-

### Beverage Abstract Class
class Beverage:
    
    def __init__(self):
        self.description = 'Unknown Beverage'
        
    def getDescription(self):
        return self.description
    
    def cost(self):
        pass

### Decorator Abstract Class
class CondimentDecorator(Beverage):
    
    def getDescription(self):
        pass
   
### Condiment
class Milk(CondimentDecorator):
    
    def __init__(self, beverage):
        self.beverage = beverage
    
    def cost(self):
        return self.beverage.cost() + .1
    
    def getDescription(self):
        return self.beverage.getDescription() + ', Milk'

class Mocha(CondimentDecorator):
    
    def __init__(self, beverage):
        self.beverage = beverage
    
    def cost(self):
        return self.beverage.cost() + .2
    
    def getDescription(self):
        return self.beverage.getDescription() + ', Mocha'
    
class Soy(CondimentDecorator):
    
    def __init__(self, beverage):
        self.beverage = beverage
    
    def cost(self):
        return self.beverage.cost() + .15
    
    def getDescription(self):
        return self.beverage.getDescription() + ', Soy'
    
class Whip(CondimentDecorator):
    
    def __init__(self, beverage):
        self.beverage = beverage
    
    def cost(self):
        return self.beverage.cost() + .1
    
    def getDescription(self):
        return self.beverage.getDescription() + ', Whip'
    
### Beverages
class HouseBlend(Beverage):
    
    def __init__(self):
        self.description = 'House Blend'
    
    def cost(self):
        return .89
    
class DarkRoast(Beverage):
    
    def __init__(self):
        self.description = 'Dark Roast'
    
    def cost(self):
        return .99

class Espresso(Beverage):
    
    def __init__(self):
        self.description = 'Espresso'
    
    def cost(self):
        return 1.99
        
class Decaf(Beverage):
    
    def __init__(self):
        self.description = 'Decaf'
    
    def cost(self):
        return 1.05
    

beverage = Espresso()
print(f'Description: {beverage.getDescription()} | Cost: {beverage.cost()} $')

beverage2 = DarkRoast()
beverage2 = Mocha(beverage2)
beverage2 = Mocha(beverage2)
beverage2 = Whip(beverage2)
print(f'Description: {beverage2.getDescription()} | Cost: {beverage2.cost()} $')

beverage3 = HouseBlend()
beverage3 = Soy(beverage3)
beverage3 = Mocha(beverage3)
beverage3 = Whip(beverage3)
print(f'Description: {beverage3.getDescription()} | Cost: {beverage3.cost()} $')
  