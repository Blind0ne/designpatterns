# coding=UTF-8
from Dough import *
from Sauce import *
from Veggies import *
from Cheese import *
from Pepperoni import *
from Clams import *

class Pizza(object):

  """
   Abstrakte Pizza Klasse

  :version:
  :author:
  """

  def prepare(self):
    pass


  def bake(self):
    print("Baking the pizza ...")


  def cut(self):
    print("Cutting the pizza ...")


  def box(self):
    print("Boxing the pizza ...")


  def setName(self, name):
    self.name = name


  def getName(self):
    return name


  def toString(self):
    pass



