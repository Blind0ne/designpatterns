# -*- coding: utf-8 -*-

class PizzaStore:
    
            
    def createPizza(self, item):
        pass
    
    def orderPizza(self, ptype):    
        
        # Factory
        pizza = self.createPizza(ptype)            
        
        pizza.prepare()
        pizza.bake()
        pizza.cut()
        pizza.box()
        return pizza
    
class NYPizzaStore(PizzaStore):
    def createPizza(self, item):
        if item == 'cheese':
            return NYStyleCheesePizza()
        elif item == 'pepperoni':
            return NYStylePepperoniPizza()
        else:
            return None

class ChicagoPizzaStore(PizzaStore):
    def createPizza(self, item):
        if item == 'cheese':
            return ChicagoStyleCheesePizza()
        elif item == 'pepperoni':
            return ChicagoStylePepperoniPizza()
        else:
            return None

class CaliforniaPizzaStore(PizzaStore):
    def createPizza(self, item):
        if item == 'cheese':
            return CaliforniaStyleCheesePizza()
        elif item == 'pepperoni':
            return CaliforniaStylePepperoniPizza()
        else:
            return None
    
class Pizza:
    name = 'Abstract Pizza'
    
    def prepare(self):
        print(f'Preparing {self.name} ...')
    
    def bake(self):
        print(f'Baking {self.name} ...')
    
    def cut(self):
        print(f'Cutting {self.name} ...')
    
    def box(self):
        print(f'Boxing {self.name} ...')
    
class NYStyleCheesePizza(Pizza):
    def __init__(self):
        self.name = 'NYStyleCheesePizza'
        print('Getting Items for NYStyleCheesePizza')
        
class ChicagoStyleCheesePizza(Pizza):
    def __init__(self):
        self.name = 'ChicagoStyleCheesePizza'
        print('Getting Items for ChicagoStyleCheesePizza')
        
class CaliforniaStyleCheesePizza(Pizza):
    def __init__(self):
        self.name = 'CaliforniaStyleCheesePizza'
        print('Getting Items for CaliforniaStyleCheesePizza')
        
class NYStylePepperoniPizza(Pizza):
    def __init__(self):
        self.name = 'NYStylePepperoniPizza'
        print('Getting Items for NYStylePepperoniPizza')
        
class ChicagoStylePepperoniPizza(Pizza):
    def __init__(self):
        self.name = 'ChicagoStylePepperoniPizza'
        print('Getting Items for ChicagoStylePepperoniPizza')
        
class CaliforniaStylePepperoniPizza(Pizza):
    def __init__(self):
        self.name = 'CaliforniaStylePepperoniPizza'
        print('Getting Items for CaliforniaStylePepperoniPizza')
    
class CheesePizza(Pizza):    
    def __init__(self):
        self.name = 'CheesePizza'
        print('Getting Items for CheesePizza')

class GreekPizza(Pizza):
    
    def __init__(self):
        self.name = 'GreekPizza'
        print('Getting Items for GreekPizza')

class PepperoniPizza(Pizza):
    def __init__(self):
        self.name = 'PepperoniPizza'
        print('Getting Items for PepperoniPizza')

class ClamPizza(Pizza):
    def __init__(self):
        self.name = 'ClamPizza'
        print('Getting Items for ClamPizza')

class VeggiePizza(Pizza):
    def __init__(self):
        self.name = 'VeggiePizza'
        print('Getting Items for VeggiePizza')
        

pizzastore = NYPizzaStore()
pizzastore.orderPizza('cheese')
pizzastore.orderPizza('pepperoni')

pizzastore = ChicagoPizzaStore()
pizzastore.orderPizza('cheese')
pizzastore.orderPizza('pepperoni')

