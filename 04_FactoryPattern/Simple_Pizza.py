# -*- coding: utf-8 -*-

class PizzaStore:
    
    def __init__(self):
        self.factory = SimplePizzaFactory()
    
    def orderPizza(self, ptype):    
        
        pizza = self.factory.createPizza(ptype)            
        pizza.prepare()
        pizza.bake()
        pizza.cut()
        pizza.box()
        return pizza

    
class Pizza:
    
    def prepare(self):
        print('Preparing ...')
    
    def bake(self):
        print('Baking ...')
    
    def cut(self):
        print('Cutting ...')
    
    def box(self):
        print('Boxing ...')
    
class CheesePizza(Pizza):    
    def __init__(self):
        print('Getting Items for CheesePizza')

class GreekPizza(Pizza):
    
    def __init__(self):
        print('Getting Items for GreekPizza')

class PepperoniPizza(Pizza):
    def __init__(self):
        print('Getting Items for PepperoniPizza')

class ClamPizza(Pizza):
    def __init__(self):
        print('Getting Items for ClamPizza')

class VeggiePizza(Pizza):
    def __init__(self):
        print('Getting Items for VeggiePizza')
        
        
class SimplePizzaFactory:
    
    @staticmethod
    def createPizza(ptype):
        pizza = None
        
        if ptype == 'cheese':
            pizza = CheesePizza()
        elif ptype == 'pepperoni':
            pizza = PepperoniPizza()
        return pizza

pizzastore = PizzaStore()
pizzastore.orderPizza('cheese')
pizzastore.orderPizza('pepperoni')

