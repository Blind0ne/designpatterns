# https://stackoverflow.com/questions/6760685/creating-a-singleton-in-python

class Singleton:
    uniqueInstance = None
    
    def __init__(self):
        pass
    
    def getInstance(self):
        if self.uniqueInstance == None:
            self.uniqueInstance = Singleton()
        return self.uniqueInstance


p = Singleton().getInstance()
print(p.getInstance())
print(p.getInstance())