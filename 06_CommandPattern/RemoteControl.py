class Command:
    '''Command Interface'''
    def execute(self):
        pass
    
    def undo(self):
        pass


class Light:

    def on(self):
        print('Light is On')

    def off(self):
        print('Light is Off')

class LightOnCommand(Command):
    '''Light command control'''
    light = Light()
    
    def __init__(self, light):
        self.light = light
        
    def __str__(self):
        return 'Light command control on'
    
    def execute(self):
        self.light.on()
        
    def undo(self):
        self.light.off()
        
class LightOffCommand(Command):
    '''Light command control'''
    light = Light()
    
    def __init__(self, light):
        self.light = light
        
    def __str__(self):
        return 'Light command control off'
    
    def execute(self):
        self.light.off()
        
    def undo(self):
        self.light.on()
    
class GarageDoor:
    def up(self):
        print('Garagedoor is open')
    
    def down(self):
        print('Garagedoor is closed')
    
    def stop(self):
        print('Garagedoor stop')
    
    def lightOn(self):
        print('Garagedoor light on')
    
    def lightOff(self):
        print('Garagedoor light off')
    
class GarageDoorOpenCommand(Command):
    garagedoor = GarageDoor()
    
    def __init__(self, garagedoor):
        self.garagedoor = garagedoor
        
    def __str__(self):
        return 'Open Garage Door Command'
        
    def execute(self):
        self.garagedoor.up()
        
    def undo(self):
        self.garagedoor.down()
        
class GarageDoorCloseCommand(Command):
    garagedoor = GarageDoor()
    
    def __init__(self, garagedoor):
        self.garagedoor = garagedoor
        
    def execute(self):
        self.garagedoor.down()
        
    def undo(self):
        self.garagedoor.up()
        
    def __str__(self):
        return 'Close Garage Door Command'
        

        
#class SimpleRemoteControl:
#    
#    def setCommand(self, command):
#        self.slot = command
#
#    def buttonWasPressed(self):
#        self.slot.execute()
        
class NoCommand(Command):
    def __str__(self):
        return 'No commands assigned'
        
class RemoteControl:
    onCommands = []
    offCommands = []
    undoCommand = Command()
    
    
    nocommand = NoCommand() 
    def __init__(self):
        for i in range(0,7):
            self.onCommands.append(self.nocommand)
            self.offCommands.append(self.nocommand)
        self.undoCommand = self.nocommand
    
    def setCommand(self, slot, onCommand, offCommand):
        self.onCommands[slot] = onCommand
        self.offCommands[slot] = offCommand

    def onButtonWasPushed(self, slot):
        self.onCommands[slot].execute()
        self.undoCommand = self.onCommands[slot]
        
    def offButtonWasPushed(self, slot):
        self.offCommands[slot].execute()
        self.undoCommand = self.offCommands[slot]
        
    def undoButtonWasPushed(self):
        self.undoCommand.undo()
        
    def __str__(self):
        stringBuff = []
        stringBuff.append('\n------ Remote Control ------\n')
        for i in range(0, len(self.onCommands)):
            stringBuff.append(f'[slot {i}] {self.onCommands[i]} - {self.offCommands[i]}\n')
            
        return ' '.join(stringBuff)
        
     
### Test the remote control
#remote = SimpleRemoteControl()
# TODO Implement Macro Party Mode
        
remote = RemoteControl()
print(remote)
gd = GarageDoor()

gdon = GarageDoorOpenCommand(gd)
gdoff = GarageDoorCloseCommand(gd)

remote.setCommand(0, gdon, gdoff)

print(remote)

remote.onButtonWasPushed(0)
remote.offButtonWasPushed(0)
remote.undoButtonWasPushed()

## Hardware to control
#light = Light()
#garagedoor = GarageDoor()
#
## Commands
#lightOn = LightOnCommand(light)
#garagedooropen = GarageDoorOpenCommand(garagedoor)
#
## Executing Commands
#remote.setCommand(lightOn)
#remote.buttonWasPressed()
#remote.setCommand(garagedooropen)
#remote.buttonWasPressed()
