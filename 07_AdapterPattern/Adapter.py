# -*- coding: utf-8 -*-
'''
This file shows how adapters can be used
'''

class Duck:
    ''' Duck Interface '''
    def quack(self):
        pass
    
    def fly(self):
        pass
    
class MallardDuck(Duck):
    def quack(self):
        print('Quack')
    
    def fly(self):
        print("I'm flying")
    
class Turkey:
    ''' Turkey Interface '''
    def gobble(self):
        pass
    
    def fly(self):
        pass
    
class WildTurkey(Turkey):
    
    def gobble(self):
        print('Gobble gobble')
        
    def fly(self):
        print("I'm flying a short distance")
        
class TurkeyAdapter(Duck):
    turkey = Turkey()
    
    def __init__(self, turkey):
        self.turkey = turkey
        
    def quack(self):
        self.turkey.gobble()
        
    def fly(self):
        for i in range(0,5):
            self.turkey.fly()
            

    
# Testing the adapter
duck = MallardDuck()
turkey = WildTurkey()

turkeyAdapter = TurkeyAdapter(turkey)

print('The turkey says ...')
turkey.gobble()
turkey.fly()

def testDuck(duckobj):
    duckobj.quack()
    duckobj.fly()

print('The duck says ...')
testDuck(duck)

print('The turkey ...')
testDuck(turkeyAdapter)
        