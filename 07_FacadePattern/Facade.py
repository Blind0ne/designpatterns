# -*- coding: utf-8 -*-
  
class Amp:
    def on(self):
        print('Turning amp on ...')
        
    def off(self):
        print('Turning amp off ...')
        
    def setDvd(self, dvd):
        print('Setting dvd ...')
        
    def setSurroundSound(self):
        print('Setting surround sound')
        
    def setVolume(self, volume):
        print(f'Setting volume to {volume}')

class Tuner:
    pass

class DvdPlayer:
    def on(self):
        print('Turning dvd on ...')
        
    def off(self):
        print('Turning dvd off ...')
        
    def play(self, movie):
        print('Playing movie ...')
        
    def stop(self):
        print('Stopping movie ...')
        
    def eject(self):
        print('Ejecting movie ...')

class CdPlayer:
    def on(self):
        print('Turning cd on ...')
        
    def play(self, cd):
        print('Playing cd ...')

class Projector:
    def on(self):
        print('Projector on')
        
    def off(self):
        print('Projector off')
        
    def wideScreenMode(self):
        print('Using wide screen mode ...')

class TheaterLights:
    
    def on(self):
        print('Turning lights on ...')
    
    def dim(self, num):
        print(f'Dimming lights by factor {num}')

class Screen:
    def up(self):
        print('Screen is going up')
        
    def down(self):
        print('Screen is going down')

class PopcornPopper:
    
    def on(self):
        print('Turning popper on ...')
        
    def off(self):
        print('Turning popper off ...')
        
    def pop(self):
        print('Popping popcorn ...')

class HomeTheaterFacade:
    amp = Amp()
    tuner = Tuner()
    dvd = DvdPlayer()
    cd = CdPlayer()
    projector = Projector()
    lights = TheaterLights()
    screen = Screen()
    popper = PopcornPopper()
    
    def __init__(self, amp,
                 tuner,
                 dvd,
                 cd,
                 projector,
                 screen,
                 lights,
                 popper):
        self.amp = amp
        self.tuner = tuner
        self.dvd = dvd
        self.cd = cd
        self.projector = projector
        self.lights = lights
        self.screen = screen
        self.popper = popper
        
    def watchMovie(self, movie):
        print('Get ready to watch a movie :) ...')
        self.popper.on()
        self.popper.pop()
        
        self.lights.dim(10)
        
        self.screen.down()
        
        self.projector.on()
        self.projector.wideScreenMode()
        
        self.amp.on()
        self.amp.setSurroundSound()
        self.amp.setVolume(5)
        self.dvd.on()
        self.dvd.play(movie)
        
    def endMovie(self, movie):
        print('Shutting movie off ...')
        self.popper.off()        
        self.lights.on()        
        self.screen.up()        
        self.projector.off()        
        self.amp.off()
        self.dvd.stop()
        self.dvd.eject()
        self.dvd.off()
  
        
amp = Amp()
tuner = Tuner()
dvd = DvdPlayer()
cd = CdPlayer()
projector = Projector()
lights = TheaterLights()
screen = Screen()
popper = PopcornPopper()
ht = HomeTheaterFacade(amp,tuner,dvd,cd,projector,screen,lights,popper)
ht.watchMovie('My movie')
print('---------')
ht.endMovie('My movie')