# -*- coding: utf-8 -*-

# Override methods if abstract classs

class CaffeeineBeverage:
    def prepareReccipe(self):
        self.boilWater()
        self.brew()
        self.pourInCup()
        self.addCondinments()

    def boilWater(self):
        print("Boiling Water ...")


    def brew(self):
        pass

    def addCondinments(self):
        pass

    def pourInCup(self):
        print("Pouring into Cup")

class Coffee(CaffeeineBeverage):

    def brew(self):
        print("Dripping coffe through filter")

    def addCondinments(self):
        print("Adding sugar and milk")

class Tea(CaffeeineBeverage):
    
    def brew(self):
        print("Steeping the tea")

    def addCondinments(self):
        print("Adding lemon")


myTea = Tea()
myTea.prepareReccipe()
print("-------------")
myCoffee = Coffee()
myCoffee.prepareReccipe()