# -*- coding: utf-8 -*-

class MenuItem:
    
    def __init__(self, name, description, vegetarian, price):
        self.name = name
        self.description = description
        self.vegetarian = vegetarian
        self.price = price
        
    def getName(self):
        return self.name
    
    def getDescription(self):
        return self.description
    
    def getPrice(self):
        return self.price
    
    def isVegetarian(self):
        return self.vegetarian
    
    def __str__(self):
        return self.name
    
class PancakeHouseMenu:
    '''Menu items are implemented as a list'''
    menuItems = []
    
    def __init__(self):
        self.addItem("K&B's Pancake Breakfast",
                "Pancakes with scambled eggs and toast",
                True,
                2.99)
        self.addItem("Regular Pancake Breakfast",
                "Pancakes with fried eggs and sausage",
                False,
                2.99)
        self.addItem("Blueberry Pancakes",
                "Pancakes made with fresh blueberries",
                True,
                3.49)
        self.addItem("Waffles",
                "Waffles with your choice of blueberries or strawberries",
                True,
                3.59)
        
    def addItem(self, name, description, vegetarian, price):
        menuitem = MenuItem(name, description, vegetarian, price)
        self.menuItems.append(menuitem)
        
    def getMenuItems(self):
        return self.menuItems
        

class DinerMenu:
    '''Menu items are implemented as an array'''
    
    MAX_ITEMS = 4
    numberOfItems = 0
    menuItems = [None]*MAX_ITEMS
    
    def __init__(self):
        self.addItem("Vegetarian BLT",
                "Bacon with lettuce",
                True,
                2.99)
        self.addItem("BLT",
                "Another Bacon with lettuce",
                False,
                2.99)
        self.addItem("Soup of the day",
                "Soup of the day, with a side of potato salad",
                False,
                3.29)
        self.addItem("Hot dog",
                "Hot dog with sauerkraut",
                False,
                3.05)
        
    def addItem(self, name, description, vegetarian, price):
        if self.numberOfItems >= self.MAX_ITEMS:
            print("Sorry, menu is full")
        else:
            menuitem = MenuItem(name, description, vegetarian, price)
            self.menuItems[self.numberOfItems] = menuitem
            self.numberOfItems += 1
        
    def getMenuItems(self):
        return self.menuItems
    
#class Iterator:
#    ''' Iterator Interface '''
#    def hasNext(self):
#        raise NotImplementedError
#    
#    def mnext(self):
#        raise NotImplementedError
#
#class DinerMenuIterator(Iterator):
#    
#    def __init__(self, items):
#        self.items = items
        
    
        
    

# Testing the waitress ...
pancakehouse = PancakeHouseMenu()
breakfastItems = pancakehouse.getMenuItems()

dinermenu = DinerMenu()
lunchItems = dinermenu.getMenuItems()

#print("--- BREAKFAST ---")
#for bi in breakfastItems:
#    print(bi.getName(), bi.getPrice(), '€')
#    
#print("--- LUNCH ---")
#for li in lunchItems:
#    print(li.getName(), li.getPrice(), '€')
    
# Pythonic solution :)
#for item in breakfastItems + lunchItems:
#    print(item.getName(), item.getPrice(), '€')












