# -*- coding: utf-8 -*-

class GumBallMachine:
    SOLD_OUT = 0
    NO_QUARTER = 1
    HAS_QUARTER = 2
    SOLD = 3
    
    state = SOLD_OUT
    count = 0
    
    def __init__(self, count):
        self.count = count
        if count > 0:
            self.state = self.NO_QUARTER
            
    def __str__(self):
        if self.state == self.HAS_QUARTER:
            stateName = 'Has Quarter'
            
        elif self.state == self.SOLD_OUT:
            stateName = 'Out of gumballs'
            
        elif self.state == self.SOLD:
            stateName = 'Gumball sold'
            
        elif self.state == self.NO_QUARTER:
            stateName = 'No Quarter'
            
        return f"------------\nMighty Gumball, Inc.\nInventory: {self.count}\nState: {stateName}\n------------"
    
    def insertQuarter(self):
        if self.state == self.HAS_QUARTER:
            print("You can't insert another quarter")
            
        elif self.state == self.SOLD_OUT:
            print("The machine is sold out")
            
        elif self.state == self.SOLD:
            print("Please wait we already giving a gumball")
            
        elif self.state == self.NO_QUARTER:
            self.state = self.HAS_QUARTER
            print("You inserted a quarter")
            
    def ejectQuarter(self):
        if self.state == self.HAS_QUARTER:
            self.state = self.NO_QUARTER
            print("Quarter returned")
            
        elif self.state == self.SOLD_OUT:
            print("You can't eject you haven't inserted a quarter yet")
            
        elif self.state == self.SOLD:
            print("Sorry you already turned the crank")
            
        elif self.state == self.NO_QUARTER:            
            print("You haven't inserted a quarter")
            
    def turnCrank(self):
        if self.state == self.HAS_QUARTER:
            self.state = self.SOLD
            print("You turned ...")
            self.dispense()
            
        elif self.state == self.SOLD_OUT:
            print("You turned but there are no gumballs")
            
        elif self.state == self.SOLD:
            print("Turning twice doesn't give you another gumball")
            
        elif self.state == self.NO_QUARTER:
            print("You turned but there is no quarter")
            
    def dispense(self):
        if self.state == self.HAS_QUARTER:
            print("No gumball dispensed")
            
        elif self.state == self.SOLD_OUT:
            print("No gumball dispensed")
            
        elif self.state == self.SOLD:
            print("A gumball comes rolling out of the slot! Enjoy!")
            self.count -= 1
            if self.count < 1:
                self.state = self.SOLD_OUT
                print("Oops out of gumballs")
            else:
                self.state = self.NO_QUARTER
            
        elif self.state == self.NO_QUARTER:
            print("You need to pay first")
            
# Gumball Machine Test Drive            
gumballMachine = GumBallMachine(5)
print(gumballMachine)

gumballMachine.insertQuarter()
gumballMachine.turnCrank()
print(gumballMachine)

gumballMachine.insertQuarter()
gumballMachine.ejectQuarter()
gumballMachine.turnCrank()
print(gumballMachine)

gumballMachine.insertQuarter()
gumballMachine.turnCrank()
gumballMachine.insertQuarter()
gumballMachine.turnCrank()
gumballMachine.ejectQuarter()
print(gumballMachine)

gumballMachine.insertQuarter()
gumballMachine.insertQuarter()
gumballMachine.turnCrank()
gumballMachine.insertQuarter()
gumballMachine.turnCrank()
gumballMachine.insertQuarter()
gumballMachine.turnCrank()
gumballMachine.insertQuarter()
gumballMachine.turnCrank()
print(gumballMachine)
