# -*- coding: utf-8 -*-

import random

class State:
    '''State interface'''
    state = None
    
    def insertQuarter(self):
        return NotImplementedError
    
    def ejectQuarter(self):
        return NotImplementedError
    
    def turnCrank(self):
        return NotImplementedError
    
    def dispense(self):
        return NotImplementedError

class SoldState(State):
    
    def __init__(self, gumballMachine):
        self.gumballMachine = gumballMachine 
        
    def insertQuarter(self):
        print("Please wait we already giving a gumball")
    
    def ejectQuarter(self):
        print("Sorry you already turned the crank")
    
    def turnCrank(self):
        print("Turning twice doesn't give you another gumball")
    
    def dispense(self):
        print("A gumball comes rolling out of the slot! Enjoy!")
        gumballMachine.setCount(gumballMachine.getCount()-1)
        if gumballMachine.getCount() < 1:
            gumballMachine.setState(gumballMachine.getSoldOutState())
            print("Oops out of gumballs")
        else:
            gumballMachine.setState(gumballMachine.getNoQuarterState())
            
    def getState(self):
        return 3

class SoldOutState(State):
    
    def __init__(self, gumballMachine):
        self.gumballMachine = gumballMachine 
        
    def insertQuarter(self):
        print("The machine is sold out")
    
    def ejectQuarter(self):
        print("You can't eject you haven't inserted a quarter yet")
    
    def turnCrank(self):
        print("You turned but there are no gumballs")
    
    def dispense(self):
        print("No gumball dispensed")

        
    def getState(self):
        return 0

class NoQuarterState(State):
    
    def __init__(self, gumballMachine):
        self.gumballMachine = gumballMachine        
        
    def insertQuarter(self):        
        print("You inserted a quarter")
        gumballMachine.setState(gumballMachine.getHasQuarterState())
    
    def ejectQuarter(self):
        print("You haven't inserted a quarter")
    
    def turnCrank(self):
        print("You turned but there is no quarter")
    
    def dispense(self):
        print("You need to pay first")
        
    def getState(self):
        return 1

class HasQuarterState(State):
    
    def __init__(self, gumballMachine):
        self.gumballMachine = gumballMachine 
        
    def insertQuarter(self):
        print("You can't insert another quarter")
    
    def ejectQuarter(self):
        print("You haven't inserted a quarter")
    
    def turnCrank(self):
        print("You turned ...")
        # 10% probabilility
        if random.randrange(1, 11)==2 and self.gumballMachine.getCount() > 1:
            gumballMachine.setState(gumballMachine.getWinnerState())
        else:
            gumballMachine.setState(gumballMachine.getSoldState())
        
    
    def dispense(self):
        print("No gumball dispensed")
        
    def getState(self):
        return 2

class WinnerState(State):
    def __init__(self, gumballMachine):
        self.gumballMachine = gumballMachine 
        
    def insertQuarter(self):
        print("Please wait we already giving two gumballs")
    
    def ejectQuarter(self):
        print("Sorry you already turned the crank")
    
    def turnCrank(self):
        print("Turning twice doesn't give you another gumball althoug you're a winner")
    
    def dispense(self):
        print("WINNER: Two gumball comes rolling out of the slot! Enjoy!")
        gumballMachine.setCount(gumballMachine.getCount()-2)
        if gumballMachine.getCount() < 1:
            gumballMachine.setState(gumballMachine.getSoldOutState())
            print("Oops out of gumballs")
        else:
            gumballMachine.setState(gumballMachine.getNoQuarterState())
        
    def getState(self):
        return 4


class GumBallMachine:    
    
    def __init__(self, count):
        # Init all states
        self.NO_QUARTER = NoQuarterState(self)
        self.HAS_QUARTER = HasQuarterState(self)
        self.SOLD_OUT = SoldOutState(self)
        self.SOLD = SoldState(self)
        self.WINNER = WinnerState(self)
        
        state = self.SOLD_OUT
    
        self.count = count
        if count > 0:
            self.state = self.NO_QUARTER
            
    def __str__(self):
        if self.state.getState() == self.HAS_QUARTER.getState():
            stateName = 'Has Quarter'
            
        elif self.state.getState() == self.SOLD_OUT.getState():
            stateName = 'Out of gumballs'
            
        elif self.state.getState() == self.SOLD.getState():
            stateName = 'Gumball sold'
            
        elif self.state.getState() == self.NO_QUARTER.getState():
            stateName = 'No Quarter'
            
        elif self.state.getState() == self.WINNER.getState():
            stateName = 'Winner'
            
        return f"------------\nMighty Gumball, Inc.\nInventory: {self.count}\nState: {stateName}\n------------"
    
    def insertQuarter(self):
        self.state.insertQuarter()
    
    def ejectQuarter(self):
        self.state.ejectQuarter()
    
    def turnCrank(self):
        self.state.turnCrank()
        self.state.dispense()
        
    def refill(self, numGumballs):
        self.count = numGumballs
        self.state = self.NO_QUARTER
        
    # Getter methods (Python don't need these but I implement them anyway :)
    def getHasQuarterState(self):
        return self.HAS_QUARTER
    
    def getNoQuarterState(self):
        return self.NO_QUARTER
    
    def getSoldOutState(self):
        return self.SOLD_OUT
    
    def getSoldState(self):
        return self.SOLD
    
    def getWinnerState(self):
        return self.WINNER
    
    def getCount(self):
        return self.count
    
    # Setter methods (Python don't need these but I implement them anyway :)
    def setState(self, state):
        self.state = state
        
    def setCount(self, newCount):
        self.count = newCount
    
        
        
# Gumball Machine Test Drive  
        
        
gumballMachine = GumBallMachine(5)
print(gumballMachine)

gumballMachine.insertQuarter()
gumballMachine.turnCrank()
print(gumballMachine)

gumballMachine.insertQuarter()
gumballMachine.ejectQuarter()
gumballMachine.turnCrank()
print(gumballMachine)

gumballMachine.insertQuarter()
gumballMachine.turnCrank()
gumballMachine.insertQuarter()
gumballMachine.turnCrank()
gumballMachine.ejectQuarter()
print(gumballMachine)

gumballMachine.insertQuarter()
gumballMachine.insertQuarter()
gumballMachine.turnCrank()
gumballMachine.insertQuarter()
gumballMachine.turnCrank()
gumballMachine.insertQuarter()
gumballMachine.turnCrank()
gumballMachine.insertQuarter()
gumballMachine.turnCrank()
print(gumballMachine)

gumballMachine.refill(6)
print(gumballMachine)
