# -*- coding: utf-8 -*-
from States.HasQuarter import HasQuarterState
from States.NoQuarter import NoQuarterState
from States.SoldOut import SoldOutState
from States.Sold import SoldState
from States.Winner import WinnerState


class GumBallMachine:
    location = ""

    def __init__(self, location, count):
        # Init all states
        self.NO_QUARTER = NoQuarterState(self)
        self.HAS_QUARTER = HasQuarterState(self)
        self.SOLD_OUT = SoldOutState(self)
        self.SOLD = SoldState(self)
        self.WINNER = WinnerState(self)

        self.location = location
        state = self.SOLD_OUT

        self.count = count
        if count > 0:
            self.state = self.NO_QUARTER

    def __str__(self):
        if self.state.getState() == self.HAS_QUARTER.getState():
            self.stateName = 'Has Quarter'

        elif self.state.getState() == self.SOLD_OUT.getState():
            self.stateName = 'Out of gumballs'

        elif self.state.getState() == self.SOLD.getState():
            self.stateName = 'Gumball sold'

        elif self.state.getState() == self.NO_QUARTER.getState():
            self.stateName = 'No Quarter'

        elif self.state.getState() == self.WINNER.getState():
            self.stateName = 'Winner'

        return f"------------\nMighty Gumball, Inc.\nInventory: {self.count}\nState: {self.stateName}\n------------"

    def insertQuarter(self):
        self.state.insertQuarter()

    def ejectQuarter(self):
        self.state.ejectQuarter()

    def turnCrank(self):
        self.state.turnCrank()
        self.state.dispense()

    def refill(self, numGumballs):
        self.count = numGumballs
        self.state = self.NO_QUARTER

    # Getter methods (Python don't need these but I implement them anyway :)
    def getHasQuarterState(self):
        return self.HAS_QUARTER

    def getNoQuarterState(self):
        return self.NO_QUARTER

    def getSoldOutState(self):
        return self.SOLD_OUT

    def getSoldState(self):
        return self.SOLD

    def getWinnerState(self):
        return self.WINNER

    def getCount(self):
        return self.count

    def getLocation(self):
        return self.location

    def getState(self):
        return self.stateName

    # Setter methods (Python don't need these but I implement them anyway :)
    def setState(self, state):
        self.state = state

    def setCount(self, newCount):
        self.count = newCount
