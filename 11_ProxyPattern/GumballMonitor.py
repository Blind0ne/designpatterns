from GumballMachine import GumBallMachine

class GumballMonitor:
    machine = GumBallMachine(0, 0)

    def __init__(self, machine):
        self.machine = machine

    def report(self):
        print(f"Gumball machine: {self.machine.getLocation()}")
        print(f"Current inventory: {self.machine.getCount()} gumballs")
        print(f"Current machine state: {self.machine.getState()}")
