from States.State import State
class SoldState(State):

    def __init__(self, gumballMachine):
        self.gumballMachine = gumballMachine

    def insertQuarter(self):
        print("Please wait we already giving a gumball")

    def ejectQuarter(self):
        print("Sorry you already turned the crank")

    def turnCrank(self):
        print("Turning twice doesn't give you another gumball")

    def dispense(self):
        print("A gumball comes rolling out of the slot! Enjoy!")
        self.gumballMachine.setCount(self.gumballMachine.getCount()-1)
        if self.gumballMachine.getCount() < 1:
            self.gumballMachine.setState(self.gumballMachine.getSoldOutState())
            print("Oops out of gumballs")
        else:
            self.gumballMachine.setState(self.gumballMachine.getNoQuarterState())

    def getState(self):
        return 3
