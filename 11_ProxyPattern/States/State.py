class State:
    '''This is a State interface which need to be implemented in subclasses'''
    state = None

    def insertQuarter(self):
        raise NotImplementedError

    def ejectQuarter(self):
        raise NotImplementedError

    def turnCrank(self):
        raise NotImplementedError

    def dispense(self):
        raise NotImplementedError
