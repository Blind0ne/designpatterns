from States.State import State
class WinnerState(State):
    def __init__(self, gumballMachine):
        self.gumballMachine = gumballMachine

    def insertQuarter(self):
        print("Please wait we already giving two gumballs")

    def ejectQuarter(self):
        print("Sorry you already turned the crank")

    def turnCrank(self):
        print("Turning twice doesn't give you another gumball althoug you're a winner")

    def dispense(self):
        print("WINNER: Two gumball comes rolling out of the slot! Enjoy!")
        gumballMachine.setCount(gumballMachine.getCount()-2)
        if gumballMachine.getCount() < 1:
            gumballMachine.setState(gumballMachine.getSoldOutState())
            print("Oops out of gumballs")
        else:
            gumballMachine.setState(gumballMachine.getNoQuarterState())

    def getState(self):
        return 4
