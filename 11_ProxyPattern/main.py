from GumballMachine import GumBallMachine
from GumballMonitor import GumballMonitor
# Gumball Machine Test Drive

# TODO implement with proxy pattern
gumballMachine = GumBallMachine("New York", 5)
print(gumballMachine)

gumballMachine.insertQuarter()
gumballMachine.turnCrank()
print(gumballMachine)

# Testing the monitor
gbm = GumballMonitor(gumballMachine)
gbm.report()

# gumballMachine.insertQuarter()
# gumballMachine.ejectQuarter()
# gumballMachine.turnCrank()
# print(gumballMachine)

# gumballMachine.insertQuarter()
# gumballMachine.turnCrank()
# gumballMachine.insertQuarter()
# gumballMachine.turnCrank()
# gumballMachine.ejectQuarter()
# print(gumballMachine)

# gumballMachine.insertQuarter()
# gumballMachine.insertQuarter()
# gumballMachine.turnCrank()
# gumballMachine.insertQuarter()
# gumballMachine.turnCrank()
# gumballMachine.insertQuarter()
# gumballMachine.turnCrank()
# gumballMachine.insertQuarter()
# gumballMachine.turnCrank()
# print(gumballMachine)

# gumballMachine.refill(6)
# print(gumballMachine)
