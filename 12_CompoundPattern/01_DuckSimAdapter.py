class Quackable:
    '''Quackable interface'''

    def quack(self):
        raise NotImplementedError


class MallardDuck(Quackable):

    def quack(self):
        print("Quack!")


class RedheadDuck(Quackable):

    def quack(self):
        print("Quack!")


class DuckCall(Quackable):

    def quack(self):
        print("Kwak!")


class RubberDuck(Quackable):

    def quack(self):
        print("Squeak!")


class Goose:
    def honk(self):
        print("Honk!")

# Adapter Pattern


class GooseAdapter(Quackable):
    goose = Goose()

    def __init__(self, goose):
        self.goose = goose

    def quack(self):
        self.goose.honk()


'''
Duck Simulation
'''
# Polymorphism function


def simulate(duck):
    duck.quack()


# With Decorators
mallardDuck = MallardDuck()
redheadDuck = RedheadDuck()
duckCall = DuckCall()
rubberDuck = RubberDuck()
gooseDuck = GooseAdapter(Goose())

print("--- Duck Simulator ---")
simulate(mallardDuck)
simulate(redheadDuck)
simulate(duckCall)
simulate(rubberDuck)
simulate(gooseDuck)