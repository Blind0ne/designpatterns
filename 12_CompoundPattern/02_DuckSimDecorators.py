class Quackable:
    '''Quackable interface'''

    def quack(self):
        raise NotImplementedError

class MallardDuck(Quackable):

    def quack(self):
        print("Quack!")    

class RedheadDuck(Quackable):
    
    def quack(self):
        print("Quack!")


class DuckCall(Quackable):

    def quack(self):
        print("Kwak!")


class RubberDuck(Quackable):

    def quack(self):
        print("Squeak!")

class Goose:
    def honk(self):
        print("Honk!")

# Adapter Pattern
class GooseAdapter(Quackable):
    goose = Goose()

    def __init__(self, goose):
        self.goose = goose

    def quack(self):
        self.goose.honk()

# Decorator Pattern
class QuackCounter(Quackable):
    numberOfQuacks = 0 # Class variable

    def __init__(self, duck):
        self.duck = duck

    
    def quack(self):
        self.duck.quack()
        QuackCounter.numberOfQuacks += 1
        # self._counter()
        
    # @classmethod
    # def _counter(cls):
    #     cls.numberOfQuacks += 1 


    def getQuacks(self):
        return self.numberOfQuacks

'''
Duck Simulation
'''
# Polymorphism function
def simulate(duck):
    duck.quack()

# With Decorators: you need to init QuackCounter for every class -> problem
mallardDuck = QuackCounter(MallardDuck())
redheadDuck = QuackCounter(RedheadDuck())
duckCall = QuackCounter(DuckCall())
rubberDuck = QuackCounter(RubberDuck())
gooseDuck = GooseAdapter(Goose())

print("--- Duck Simulator ---")
simulate(mallardDuck)
simulate(mallardDuck)
simulate(mallardDuck)
simulate(redheadDuck)
simulate(duckCall)
simulate(rubberDuck)
simulate(gooseDuck)

print(QuackCounter.numberOfQuacks, "quacks were counted")
# print(QuackCounter.__dict__)
# print(QuackCounter.getQuacks(QuackCounter))



