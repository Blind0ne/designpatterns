
class Quackable:
    '''Quackable interface'''

    def quack(self):
        raise NotImplementedError


class MallardDuck(Quackable):

    def quack(self):
        print("Quack!")


class RedheadDuck(Quackable):

    def quack(self):
        print("Quack!")


class DuckCall(Quackable):

    def quack(self):
        print("Kwak!")


class RubberDuck(Quackable):

    def quack(self):
        print("Squeak!")


class Goose:
    def honk(self):
        print("Honk!")


'''
------------------------------------------
Adapter Pattern
------------------------------------------
'''
class GooseAdapter(Quackable):
    goose = Goose()

    def __init__(self, goose):
        self.goose = goose

    def quack(self):
        self.goose.honk()


'''
------------------------------------------
Decorator Pattern
------------------------------------------
'''
class QuackCounter(Quackable):
    numberOfQuacks = 0  # Class variable

    def __init__(self, duck):
        self.duck = duck

    def quack(self):
        self.duck.quack()
        QuackCounter.numberOfQuacks += 1
        # self._counter()

    # @classmethod
    # def _counter(cls):
    #     cls.numberOfQuacks += 1

    def getQuacks(self):
        return self.numberOfQuacks


'''
------------------------------------------
Factory Pattern
------------------------------------------
'''
class AbstractDuckFactory:

    def createMallardDuck(self):
        pass

    def createRedheadDuck(self):
        pass

    def createDuckCall(self):
        pass

    def createRubberDuck(self):
        pass
    
# DUCKS
class DuckFactory(AbstractDuckFactory):
    def createMallardDuck(self):
        return MallardDuck()

    def createRedheadDuck(self):
        return RedheadDuck()

    def createDuckCall(self):
        return DuckCall()

    def createRubberDuck(self):
        return RubberDuck()


class CountingDuckFactory(AbstractDuckFactory):
    def createMallardDuck(self):
        return QuackCounter(MallardDuck())

    def createRedheadDuck(self):
        return QuackCounter(RedheadDuck())

    def createDuckCall(self):
        return QuackCounter(DuckCall())

    def createRubberDuck(self):
        return QuackCounter(RubberDuck())

class AbstractGeeseFactory:
    def createGoose(self):
        pass

# GOOSE
class GooseFactory(AbstractGeeseFactory):
    def createGoose(self):
        return GooseAdapter(Goose())


class CountingGooseFactory(AbstractGeeseFactory):
    def createGoose(self):
        return QuackCounter(GooseAdapter(Goose()))

'''
------------------------------------------
Duck Simulation
------------------------------------------
'''
# Polymorphism function
def simulate(duck):
    duck.quack()

def simulator(duckFactory, gooseFactory):

    # Better create iterator for the object creation
    mallardDuck = duckFactory.createMallardDuck()
    redheadDuck = duckFactory.createRedheadDuck()
    duckCall = duckFactory.createDuckCall()
    rubberDuck = duckFactory.createRubberDuck()

    gooseDuck = gooseFactory.createGoose()

    print("Simulator with abstract factory")
    simulate(mallardDuck)
    simulate(mallardDuck)
    simulate(mallardDuck)
    simulate(redheadDuck)
    simulate(duckCall)
    simulate(rubberDuck)
    simulate(gooseDuck)

    print(QuackCounter.numberOfQuacks, "quacks were counted")

duckFactory = CountingDuckFactory()
gooseFactory = CountingGooseFactory()
simulator(duckFactory, gooseFactory)



