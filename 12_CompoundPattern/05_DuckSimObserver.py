import abc

'''
------------------------------------------
Observer Pattern
------------------------------------------
'''


class QuackObserverable:

    def registerObserver(self, o):
        pass

    def notifyObservers(self):
        pass


class Observebale(QuackObserverable):
    # Observers should be unique
    observers = set()

    def __init__(self, duck):
        self.duck = duck
        # self.observers = []

    def registerObserver(self, o):
        self.observers.add(o)

    def notifyObservers(self):
        if len(self.observers) > 0:
            for observer in self.observers:
                observer.update(self.duck)
        else:
            print('No observers to notify')


class Observer:
    def update(self, duck):
        pass

class Quackologist(Observer):

    def update(self, duck):
        print(f"Quackologist: {duck} just quacked")

'''
------------------------------------------
Duck Classes
------------------------------------------
'''
class Quackable(QuackObserverable):
    '''Quackable interface'''

    def quack(self):
        raise NotImplementedError


class MallardDuck(Quackable):

    def __init__(self):
        self.observebale = Observebale(self)

    def __str__(self):
        return "MallardDuck"

    def quack(self):
        print("Quack!")
        self.notifyObservers()

    def registerObserver(self, o):
        self.observebale.registerObserver(o)

    def notifyObservers(self):
        self.observebale.notifyObservers()


class RedheadDuck(Quackable):

    def __init__(self):
        self.observebale = Observebale(self)

    def __str__(self):
        return "RedheadDuck"

    def quack(self):
        print("Quack!")
        self.notifyObservers()

    def registerObserver(self, o):
        self.observebale.registerObserver(o)

    def notifyObservers(self):
        self.observebale.notifyObservers()


class DuckCall(Quackable):

    def __init__(self):
        self.observebale = Observebale(self)

    def __str__(self):
        return "DuckCall"

    def quack(self):
        print("Kwak!")
        self.notifyObservers()

    def registerObserver(self, o):
        self.observebale.registerObserver(o)

    def notifyObservers(self):
        self.observebale.notifyObservers()


class RubberDuck(Quackable):

    def __init__(self):
        self.observebale = Observebale(self)

    def __str__(self):
        return "RubberDuck"

    def quack(self):
        print("Squeak!")
        self.notifyObservers()

    def registerObserver(self, o):
        self.observebale.registerObserver(o)

    def notifyObservers(self):
        self.observebale.notifyObservers()


class Goose:
    def honk(self):
        print("Honk!")


'''
------------------------------------------
Adapter Pattern
------------------------------------------
'''


class GooseAdapter(Quackable):
    goose = Goose()

    def __init__(self, goose):
        self.goose = goose
        self.observebale = Observebale(self)

    def __str__(self):
        return "Goose"

    def quack(self):
        self.goose.honk()
        self.notifyObservers()

    def registerObserver(self, o):
        self.observebale.registerObserver(o)

    def notifyObservers(self):
        self.observebale.notifyObservers()


'''
------------------------------------------
Decorator Pattern
------------------------------------------
'''


class QuackCounter(Quackable):
    numberOfQuacks = 0  # Class variable

    def __init__(self, duck):
        self.duck = duck
        self.observebale = Observebale(self)

    # def __str__(self):
    #     return str(self.duck)

    def quack(self):
        self.duck.quack()
        QuackCounter.numberOfQuacks += 1
        # self.notifyObservers()

    def registerObserver(self, o):
        self.observebale.registerObserver(o)

    def notifyObservers(self):
        self.observebale.notifyObservers()

    def getQuacks(self):
        return self.numberOfQuacks


'''
------------------------------------------
Factory Pattern
------------------------------------------
'''


class AbstractDuckFactory(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def createMallardDuck(self):
        pass

    @abc.abstractmethod
    def createRedheadDuck(self):
        pass

    @abc.abstractmethod
    def createDuckCall(self):
        pass

    @abc.abstractmethod
    def createRubberDuck(self):
        pass

# DUCKS


class DuckFactory(AbstractDuckFactory):
    def createMallardDuck(self):
        return MallardDuck()

    def createRedheadDuck(self):
        return RedheadDuck()

    def createDuckCall(self):
        return DuckCall()

    def createRubberDuck(self):
        return RubberDuck()


class CountingDuckFactory(AbstractDuckFactory):
    def createMallardDuck(self):
        return QuackCounter(MallardDuck())

    def createRedheadDuck(self):
        return QuackCounter(RedheadDuck())

    def createDuckCall(self):
        return QuackCounter(DuckCall())

    def createRubberDuck(self):
        return QuackCounter(RubberDuck())


class AbstractGeeseFactory(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def createGoose(self):
        pass

# GOOSE


class GooseFactory(AbstractGeeseFactory):
    def createGoose(self):
        return GooseAdapter(Goose())


class CountingGooseFactory(AbstractGeeseFactory):
    def createGoose(self):
        return QuackCounter(GooseAdapter(Goose()))


'''
------------------------------------------
Composite/Iterator Pattern
------------------------------------------
'''


class Flock(Quackable):

    def __init__(self):
        self.quackers = []
        # self.observebale = Observebale(self)

    def add(self, quacker):
        self.quackers.append(quacker)

    def quack(self):
        for quacker in self.quackers:
            quacker.quack()
            self.notifyObservers()

    def registerObserver(self, o):
        for quacker in self.quackers:
            quacker.registerObserver(o)

    # def notifyObservers(self):
    #     self.observebale.notifyObservers()


# BAD DESIGN BECAUSE OF INFINITE RECURSION
# class Flock(Quackable):
#     quackers = []

#     def add(self, quacker):
#         self.quackers.append(quacker)

#     def quack(self):
#         for quacker in self.quackers:
#             quacker.quack()



'''
------------------------------------------
Duck Simulation
------------------------------------------
'''
# Polymorphism function


def simulate(duck):
    duck.quack()


def simulator(duckFactory, gooseFactory):

    # Ducks
    mallardDuck = duckFactory.createMallardDuck()
    redheadDuck = duckFactory.createRedheadDuck()
    duckCall = duckFactory.createDuckCall()
    rubberDuck = duckFactory.createRubberDuck()

    gooseDuck = gooseFactory.createGoose()

    flockOfDucks = Flock()
    flockOfDucks.add(mallardDuck)
    flockOfDucks.add(redheadDuck)
    flockOfDucks.add(duckCall)
    flockOfDucks.add(rubberDuck)
    flockOfDucks.add(gooseDuck)

    # Family of Mallards
    mallardDuckOne = duckFactory.createMallardDuck()
    mallardDuckTwo = duckFactory.createMallardDuck()
    mallardDuckThree = duckFactory.createMallardDuck()
    mallardDuckFour = duckFactory.createMallardDuck()

    flockOfMallards = Flock()
    flockOfMallards.add(mallardDuckOne)
    flockOfMallards.add(mallardDuckTwo)
    flockOfMallards.add(mallardDuckThree)
    flockOfMallards.add(mallardDuckFour)

    flockOfDucks.add(flockOfMallards)


    # Register Observers
    print("Simulator: Whole Flock Simulation with Observer")
    quackologist = Quackologist()
    flockOfDucks.registerObserver(quackologist)    
    simulate(flockOfDucks)

    # print("\nSimulator: Mallard Flock Simulation")
    # simulate(flockOfMallards)

    print(QuackCounter.numberOfQuacks, "quacks were counted")


duckFactory = CountingDuckFactory()
gooseFactory = CountingGooseFactory()
simulator(duckFactory, gooseFactory)
