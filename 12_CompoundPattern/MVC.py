class BeatModelInterface:

    def initialize(self):
        pass

    def on(self):
        pass

    def off(self):
        pass

    def setBPM(self, bmp):
        pass

    def getBPM(self):
        pass

    def registerObserverBeat(self, o):
        pass

    def removeObserverBeat(self, o):
        pass

    def registerObserverBPM(self, o):
        pass

    def removeObserverBPM(self, o):
        pass

# TODO implement BeatModel
class BeatModel(BeatModelInterface):
    pass

# TODO implement View GUI (maybe in kivy)