# Design Patterns in Python
#### Implementation of Design Patterns in python from the book "Head First Design Patterns" by Eric Freeman and Elisabeth Freeman


My goal is to learn design patterns of object oriented programming. To exercise I am trying to implement the examples from the book in python. By no means my implementations are correct. I am still learning and experimenting. You can open an issue to hint me where improvement can be done or just educate me with a small comment :). Thanks in advance.

The png class diagrams like the below Duck class are created with pyreverse from the pylint package. I am also experimenting with umbrello to generate code (*.xmi files).

## Strategy Pattern
![alt text](01_StrategyPattern/classes.png)

## Observer Pattern
![alt text](02_ObserverPattern/classes.png)

## Decorator Pattern
![alt text](03_DecoratorPattern/classes.png)

## Factory Pattern
![alt text](04_FactoryPattern/classes.png)

## Singleton Pattern
![alt text](05_SingletonPattern/classes.png)

## Command Pattern
![alt text](06_CommandPattern/classes.png)

## Adapter Pattern
![alt text](07_AdapterPattern/classes.png)

## Facade Pattern
![alt text](07_FacadePattern/classes.png)

## Template Method Pattern
![alt text](08_TemplateMethodPattern/classes.png)

## State Pattern
![alt text](10_StatePattern/classes.png)

## Compound Pattern
![alt text](12_CompoundPattern/classes.png)